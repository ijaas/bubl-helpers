var sleep = require('await-sleep');
var index = require('../dist/index.js');

async function asyncPool(concurrency, arr, callback) {

    const pool = [];

    const executing = new Set();

    for (let index = 0; index < arr.length; index++) {

        const p = Promise.resolve().then(() => callback(arr[index], index, arr));

        pool.push(p);
        executing.add(p);

        const clean = () => executing.delete(p);

        p.then(clean).catch(clean);

        if (executing.size >= concurrency) {
            await Promise.race(executing);
        }

    }

    return Promise.all(pool);

}




const run = async () => {

    console.log("start");

    const timeout = i => new Promise(resolve => {
        console.log("start", i);
        setTimeout(() => {
            console.log("end", i);
            resolve(i)
        }, i)
    });

    await asyncPool(3, [1000, 5000, 3000, 2000], timeout);
    console.log("done");

};

run();
