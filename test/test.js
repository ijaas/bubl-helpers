'use strict';
var expect = require('chai').expect;
var sleep = require('await-sleep');
var index = require('../dist/index.js');

describe('loopAsync test', () => {
    it('should be in order', async () => {

        await index.loopAsync([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], async (item, index) => {

            console.log(index, item);

        }, 4);

        expect(true).to.equal(true);
    });
});

describe('loopAsyncParrallel test', () => {
    it('should be in random order', async () => {

        await index.loopAsyncPool(4, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], async (item, index, arr) => {

            console.log("running", index, (10 - index) * 10);
            await sleep((10 - index) * 10);
            console.log("ending", index, item);

        });

        expect(true).to.equal(true);
    });
});

// describe('fixedNum test', () => {
//     it('should return 23', () => {
//         var result = index.fixedNum(23.445831329940283, 0);
//         console.log(result);
//         expect(result).to.equal(23);
//     });
// });

// describe('fixedNum test', () => {
//     it('should return 23', () => {
//         var result = index.fixedNum(23.087558380714782, 0);
//         console.log(result);
//         expect(result).to.equal(23);
//     });
// });

// describe('parseNum test', () => {
//     it('should return 6000', () => {
//         var result = index.parseNum('$ 6,000');
//         expect(result).to.equal(6000);
//     });
// });

// describe('formatNum test', () => {
//     it('should return $ 6,000.44', () => {
//         var result = index.formatNum('6,000.44444', 2, "$ ");
//         expect(result).to.equal("$ 6,000.44");
//     });
// });

// describe('sentenceCase test', () => {
//     it('should return Hello world', () => {
//         var result = index.sentenceCase('hello world');
//         expect(result).to.equal("Hello world");
//     });
// });

// describe('titleCase test', () => {
//     it('should return Hello World', () => {
//         var result = index.titleCase('hello world');
//         expect(result).to.equal("Hello World");
//     });
// });

// describe('slug test', () => {
//     it('should return hello-wrold', () => {
//         var result = index.slug('Hello W/O\\RL/D?#][');
//         expect(result).to.equal("hello-world");
//     });
// });

// describe('base64 test', () => {
//     it('should return hello-wrold', () => {
//         var result = index.base64Encode('Hello World');
//         result = index.base64Decode(result);
//         expect(result).to.equal("Hello World");
//     });
// });

// describe('get test', () => {
//     it('should return 0', () => {
//         const data = { zero: 0 };
//         var result = index.get(data, "zero");
//         expect(result).to.equal(data.zero);
//     });
// });