#  bubl-helpers
Bubl Helper Functions

## Installation
```sh
npm install bubl-helpers --save
```

## Functions
```sh
parseNum
formatNum
hash
date
time
dateTime
dateDay
dateWeek
dateMonth
dateYear
base64Encode
base64Decode
get
```
