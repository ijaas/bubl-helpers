import slugify from 'slugify'

import * as setValue from 'lodash/set'
import * as getValue from 'lodash/get'
import * as mergeObjects from 'lodash/merge'

import * as _moment from 'moment'
import * as objectHash from 'object-hash'
import base64 from './base64'

let logger: any = console;

/**
 * @param  {} newLogger
 */
export function setLogger(newLogger) {

    logger = newLogger;

}

export { logger };


/**
 * @param  {any} str
 * @returns number
 */
export function parseNum(str: any): number {

    if (!str) return 0;

    if (typeof str == "number") return str;

    if (typeof str.replace == 'function') str = Number(str.replace(/[^\d\.-]/g, ''));

    return parseFloat(str);

}


/**
 * @param  {any} str
 * @returns string
 */
export function parseExcelNum(str: any): string {

    // Scientific Notation to a String

    try {

        if (!str) return "";

        if (typeof str === "string") str = Number(str);

        str = str.toLocaleString('fullwide', { useGrouping: false });

        return str;

    } catch (e) {

        console.error(e);

    }

    return "";

}

/**
 * @param  {string|number} str
 * @param  {number=0} decimals
 * @param  {string=""} prefix
 * @param  {string=""} suffix
 * @returns string
 */
export function formatNum(str: string | number, decimals: boolean | number | undefined | null | "auto" = undefined, prefix: string = "", suffix: string = ""): string {

    let num = parseNum(str);

    let prettyDecimals = false;

    if (typeof decimals === "number" && decimals < 0) {
        decimals = Math.abs(decimals);
        prettyDecimals = true;
    }

    if (typeof decimals === "number") {
        num = Number(num.toFixed(decimals));
    }

    //adds commas
    let numStr = num + '';

    const split = numStr.split('.');
    const expression = /(\d+)(\d{3})/;

    let partA = split[0];
    let partB = split.length > 1 ? '.' + split[1] : '';

    if (!partB && typeof decimals === "number" && decimals && !prettyDecimals) partB = String(".").padEnd(decimals + 1, '0');

    while (expression.test(partA)) {
        partA = partA.replace(expression, '$1' + ',' + '$2');
    }

    numStr = partA + partB;

    return prefix + numStr + suffix;

}

/**
 * @param  {string|object|any} address
 * @param  {string="\n"} seperator
 * @returns string
 */
export function formatAddress(address: string | object | any, seperator: string = "\n"): string {

    let str = "";

    if (typeof address === "object") {

        const cityZip = [address.city, address.zipcode].filter(Boolean).join(", ");

        const stateCountry = [address.stateLong || address.state, address.countryLong || address.country].filter(Boolean).join(", ");

        const arr = [
            address.name,
            address.line1,
            address.line2,
            cityZip,
            stateCountry,
        ];

        str = arr.filter(Boolean).join(seperator);

    } else if (typeof address === "string") {

        str = address;

    }

    return str;

}


/**
 * @param  {string|number} num
 * @param  {number=2} decimals
 * @returns number
 */
export function fixedNum(num: string | number, decimals: number = 2): number {

    if (typeof num === "string") num = parseNum(num);
    if (typeof num === "undefined" || num === null) num = 0;

    num = Number(num.toFixed(decimals));

    return num;

}


/**
 * @param  {any} value
 * @returns string
 */
export function hash(value: any): string {

    if (value === null || value === undefined) value = "";

    if (typeof value === "object" || Array.isArray(value)) value = JSON.stringify(value);

    return objectHash(value);

}


/**
 * @param  {any} value
 * @returns string
 */
export function hashCode(value: any): string {

    if (value === null || value === undefined) value = "";

    if (typeof value === "object" || Array.isArray(value)) value = pack(value);

    let hash;

    for (let i = 0; i < value.length; i++) {
        hash = Math.imul(31, hash) + value.charCodeAt(i) | 0;
    }

    return hash ? hash.toString() : "";

}


/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export function date(str: any, outputFormat: string = '', inputFormat: string = ''): string {

    const date = _moment(str, inputFormat);

    return date.format(outputFormat || "D MMM YYYY");

}

/**
 * @param  {any} str
 * @returns string | Date
 */
export function osDateJson(date): string | Date {

    if (typeof date === "string") date = new Date(date);

    return process.platform === "win32" ? date : date.toJSON();

}


/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export function time(str: any, outputFormat: string = '', inputFormat: string = ''): string {

    const time = _moment(str, inputFormat);

    return time.format(outputFormat || "h:mm A");

}


/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export function dateTime(str: any, outputFormat: string = '', inputFormat: string = ''): string {

    const time = _moment(str, inputFormat);

    return time.format(outputFormat || "D MMM YYYY h:mm A");

}


/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export function dateTimeZone(str: any, outputFormat: string = '', inputFormat: string = ''): string {

    const time = _moment(str, inputFormat);

    let tz: any = "";

    if (typeof str === "string") {
        tz = str.substr(-6);
    }

    return time.utcOffset(tz).format(outputFormat || "D MMM YYYY h:mm A Z");

}


/**
 * @param  {any} str
 * @returns string
 */
export function dateDay(str: any): string {

    const date = _moment(str).format("YYYY-MM-DD") + "T00:00:00+00:00";

    return date;

}


/**
 * @param  {any} str
 * @returns string
 */
export function dateWeek(str: any): string {

    const date = _moment(str).startOf('week').format("YYYY-MM-DD") + "T00:00:00+00:00";

    return date;

}


/**
 * @param  {any} str
 * @returns string
 */
export function dateMonth(str: any): string {

    const date = _moment(str).startOf('month').format("YYYY-MM-DD") + "T00:00:00+00:00";

    return date;

}


/**
 * @param  {any} str
 * @returns string
 */
export function dateYear(str: any): string {

    const date = _moment(str).startOf('year').format("YYYY-MM-DD") + "T00:00:00+00:00";

    return date;

}


/**
 * @param  {string} str
 * @returns string
 */
export function base64Encode(str: string): string {

    return base64.encode(str);

}


/**
 * @param  {string} str
 * @returns string
 */
export function base64Decode(str: string): string {

    return base64.decode(str);

}


/**
 * @param  {object} object
 * @param  {string} prop
 * @param  {any} def
 * @returns any
 */
export function get(object: object, prop: string, def: any = undefined): any {

    let current = object || {};

    if (!current || !prop || typeof prop != "string") return def;

    //lodash
    current = getValue(object, prop);

    // prop.split(".").forEach(function (key) {

    //     if (current && typeof current === "object" && current[key]) current = current[key];
    //     else current = undefined;

    // });

    if (typeof current === "boolean" || typeof current === "number") return current;

    return current || def;

}


/**
 * @param  {object} object
 * @param  {string} prop
 * @param  {any} value
 * @returns any
 */
export function set(object: object, prop: string, value: any): void {

    //lodash
    setValue(object, prop, value);

}

/**
 * @param  {Array<any>} objects
 * @returns any
 */
export function merge(objectA: any, objectB: any, objectC?: any, objectD?: any, objectE?: any, objectF?: any): any {

    const output = {};

    //lodash
    mergeObjects(output, objectA, objectB || {}, objectC || {}, objectD || {}, objectE || {}, objectF || {});

    return output;

}


/**
 * @param  {string} message
 * @param  {object} extra
 * @returns any
 */
export function error(message: string, extra?: object, limit?: 8): any {

    Error.stackTraceLimit = limit || 8;

    Error.captureStackTrace(this, this.constructor);

    this.message = message;

    if (typeof extra == "object") {

        Object.keys(extra).map(key => {

            this[key] = extra[key];

        });

    }

}


/**
 * @param  {any} arrayOrObject
 * @param  {Function} callback
 */
export async function loopAsync(arrayOrObject: any, callback: Function) {

    if (Array.isArray(arrayOrObject)) {

        const arr = arrayOrObject;

        for (let index = 0; index < arr.length; index++) {

            await callback(arr[index], index, arr);

        }

    } else if (typeof arrayOrObject === "object" || typeof arrayOrObject === "function") {

        const obj = typeof arrayOrObject === "function" ? Object(arrayOrObject) : arrayOrObject;
        const arr = Object.keys(obj);

        for (let index = 0; index < arr.length; index++) {

            await callback(obj[arr[index]], arr[index], obj);

        }

    }

}


/**
 * @param  {number=2} concurrency
 * @param  {Array<any>} arr
 * @param  {Function} callback
 */
export async function loopAsyncPool(concurrency: number = 2, arr: Array<any>, callback: Function) {

    const pool = [];

    const executing = new Set();

    for (let index = 0; index < arr.length; index++) {

        const p = Promise.resolve().then(() => callback(arr[index], index, arr));

        pool.push(p);
        executing.add(p);

        const clean = () => executing.delete(p);

        p.then(clean).catch(clean);

        if (executing.size >= concurrency) {
            await Promise.race(executing);
        }

    }

    return Promise.all(pool);

}

//Alias for backword compatibility
let forEach = loopAsync;
export { forEach };


/**
 * @param  {any} arrayOrObject
 * @param  {Function} callback
 */
export function loop(arrayOrObject: any, callback: Function) {

    if (Array.isArray(arrayOrObject)) {

        const arr = arrayOrObject;

        for (let index = 0; index < arr.length; index++) {

            callback(arr[index], index, arr);

        }

    } else if (typeof arrayOrObject === "object") {

        const obj = arrayOrObject;
        const arr = Object.keys(obj);

        for (let index = 0; index < arr.length; index++) {

            callback(obj[arr[index]], arr[index], obj);

        }

    } else if (typeof arrayOrObject === "function") {

        const obj = Object(arrayOrObject);
        const arr = Object.keys(obj);

        for (let index = 0; index < arr.length; index++) {

            callback(obj[arr[index]], arr[index], obj);

        }

    }

}


/**
 * @param  {string} str
 * @returns string
 */
export function upperCase(str: string): string {

    return str ? str.toUpperCase() : str;

}


/**
 * @param  {string} str
 * @returns string
 */
export function lowerCase(str: string): string {

    return str ? str.toLowerCase() : str;
}


/**
 * @param  {string} str
 * @returns string
 */
export function sentenceCase(str: string): string {

    return str ? str[0].toUpperCase() + str.slice(1) : str;

}


/**
 * @param  {string} str
 * @returns string
 */
export function titleCase(str: string): string {

    return str ? str.split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ') : str;

}

/**
 * @param  {string} str
 * @returns string
 */
export function startCase(str: string): string {

    return str.replace(/([A-Z]+)/g, " $1").replace(/([A-Z][a-z])/g, " $1");

}


/**
 * @param  {string} str
 * @param  {object} options?
 * @returns str
 */
export function slug(str: string, options?: object): string {

    let defaults = {
        replacement: '-',
        lower: true,
        remove: /[*+~.()'"!:@#?^\?\/\\\[\]]/g,
    }

    if (options) defaults = {
        ...defaults,
        ...options
    }

    return str && typeof str === "string" ? slugify(str, defaults) : str;

}

/**
 * @param  {string} str
 * @returns str
 */
export function unslug(str: string): string {

    str = str
        .replace(/\-/g, " ")
        .replace(/\w\S*/g, (text) => text.charAt(0).toUpperCase() + text.slice(1).toLowerCase());

    return str;

}


/**
 * @param  {Function} callback
 * @param  {number=1} delay
 */
export async function bgTask(callback: Function, delay: number = 1) {

    setTimeout(async () => {

        try {

            await callback();

        } catch (e) {

            if (logger && typeof logger.log === "function") logger.log(e);
            else console.log(e);

        }

    }, delay);

}


/**
 * @param  {Array<any>} data
 * @param  {string=''} parentIdKey
 * @returns Array
 */
export function treeSort(data, parentIdKey) {

    const all = [...data];
    const list: Array<any> = [];
    const idExists: any = {};
    const children: any = {};

    const mapDeep = (record, index, order = false) => {

        record.children = [];

        if (order) record.__order = index + 2;

        loop(children[record.id], (child, childIndex) => {

            if (child) record.children.push(mapDeep(child, childIndex));

        });

        return record;

    }

    loop(data, (record, index) => {

        if (record) idExists[record.id] = true;

    });

    loop(data, (record, index) => {

        if (record) {

            const parentId = record[parentIdKey];

            if (parentId && idExists[record.parentId]) {

                if (!children[parentId]) children[parentId] = [];

                children[parentId].push(record);

                all[index] = null;

            }

        }

    });

    const parents = all.filter(Boolean);

    loop(parents, (record, index) => {

        if (record) list.push(mapDeep(record, index));

    });

    return list;

}

/**
 * @param  {number} n
 * @param  {string} single
 * @param  {string} multiple
 * @param  {string=null} zero
 */
export function howMany(n: number, single: string, multiple: string, zero: string = null) {

    let str = zero || multiple || single;

    if (n === 1) str = single;
    else if (n > 1) str = multiple;

    return str.replace("%s", n.toString());

}

/**
 * @param  {number} num
 * @returns string
 */
export function numberToLetter(num: number): string {

    let letters = '';

    while (num >= 0) {
        letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[num % 26] + letters
        num = Math.floor(num / 26) - 1
    }

    return letters;

}

/**
 * @param  {string} str
 * @returns number
 */
export function letterToNumber(str: string): number {

    var chrs = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ', mode = chrs.length - 1, number = 0;

    for (let p = 0; p < str.length; p++) {

        number = number * mode + chrs.indexOf(str[p]);

    }

    return number;

}


/**
 * @param  {string} str
 * @returns string
 */
export function nl2br(str: string): string {

    if (typeof str === 'undefined' || str === null) {
        return '';
    }

    var breakTag = '<br />';

    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');

}

/**
 * Check if given value is empty
 * @param  {any} value
 * @returns boolean
 */
export function isEmpty(value: any): boolean {

    //if null or undefined = empty
    if (value === null || value === undefined) return true;

    //if array with length 0 = empty
    if (value.hasOwnProperty('length') && value.length === 0) return true;

    //if string of blank spaces = empty
    if (typeof value === "string" && !value.trim()) return true;

    //if object with no keys = empty
    if (value.constructor === Object && Object.keys(value).length === 0) return true;

    // = not empty
    return false;

}

/**
 * @param  {Object|Array<any>|any} object
 * @returns Object|Array<any>|any
 */
export function clone(object: Object | Array<any> | any): Object | Array<any> | any {

    if (typeof structuredClone === "function") return structuredClone(object);
    else return JSON.parse(JSON.stringify(object));

}

/**
 * @param  {any} object
 * @returns string
 */
export function pack(value: any): string {

    return JSON.stringify(value);

}


/**
 * @param  {string} object
 * @returns any
 */
export function unpack(value: string): any {

    return JSON.parse(value);

}