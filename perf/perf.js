import large from "./large.json" assert { type: 'json' };
import small from "./small.json" assert { type: 'json' };
import objectHash from 'object-hash'
import equal from 'deep-equal'

const times = 100;
const data = small;

const profile = (name, times, callback) => {

    console.log("--------------------");

    var start = Date.now();

    for (let i = 0; i < times; i++) {

        callback();

    }

    var end = Date.now();

    console.log(name);
    console.log(`Total Time: ${end - start}ms`);
    console.log(`Average Time: ${(end - start) / times}ms`);

}

const memData = structuredClone(data);
const memStr = JSON.stringify(data);
const memHash = objectHash(JSON.stringify(data));

// profile("Deep Equal", times, () => {

//     if (equal(data, memData)) {
//     }

// });


profile("Stringify", times, () => {

    const str = JSON.stringify(data);
    if (str === memStr) {
    }

});

profile("Stringify Hash", times, () => {

    const hash = objectHash(JSON.stringify(data));
    if (hash === memHash) {
    }

});

profile("Stringify Hash Code", times, () => {

    const hash = hashCode(JSON.stringify(data));
    if (hash === memHash) {
    }

});

function hashCode(s) {
    let h;
    for (let i = 0; i < s.length; i++)
        h = Math.imul(31, h) + s.charCodeAt(i) | 0;

    return h.toString();
}