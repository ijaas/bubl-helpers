declare let logger: any;
/**
 * @param  {} newLogger
 */
export declare function setLogger(newLogger: any): void;
export { logger };
/**
 * @param  {any} str
 * @returns number
 */
export declare function parseNum(str: any): number;
/**
 * @param  {any} str
 * @returns string
 */
export declare function parseExcelNum(str: any): string;
/**
 * @param  {string|number} str
 * @param  {number=0} decimals
 * @param  {string=""} prefix
 * @param  {string=""} suffix
 * @returns string
 */
export declare function formatNum(str: string | number, decimals?: boolean | number | undefined | null | "auto", prefix?: string, suffix?: string): string;
/**
 * @param  {string|object|any} address
 * @param  {string="\n"} seperator
 * @returns string
 */
export declare function formatAddress(address: string | object | any, seperator?: string): string;
/**
 * @param  {string|number} num
 * @param  {number=2} decimals
 * @returns number
 */
export declare function fixedNum(num: string | number, decimals?: number): number;
/**
 * @param  {any} value
 * @returns string
 */
export declare function hash(value: any): string;
/**
 * @param  {any} value
 * @returns string
 */
export declare function hashCode(value: any): string;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export declare function date(str: any, outputFormat?: string, inputFormat?: string): string;
/**
 * @param  {any} str
 * @returns string | Date
 */
export declare function osDateJson(date: any): string | Date;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export declare function time(str: any, outputFormat?: string, inputFormat?: string): string;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export declare function dateTime(str: any, outputFormat?: string, inputFormat?: string): string;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
export declare function dateTimeZone(str: any, outputFormat?: string, inputFormat?: string): string;
/**
 * @param  {any} str
 * @returns string
 */
export declare function dateDay(str: any): string;
/**
 * @param  {any} str
 * @returns string
 */
export declare function dateWeek(str: any): string;
/**
 * @param  {any} str
 * @returns string
 */
export declare function dateMonth(str: any): string;
/**
 * @param  {any} str
 * @returns string
 */
export declare function dateYear(str: any): string;
/**
 * @param  {string} str
 * @returns string
 */
export declare function base64Encode(str: string): string;
/**
 * @param  {string} str
 * @returns string
 */
export declare function base64Decode(str: string): string;
/**
 * @param  {object} object
 * @param  {string} prop
 * @param  {any} def
 * @returns any
 */
export declare function get(object: object, prop: string, def?: any): any;
/**
 * @param  {object} object
 * @param  {string} prop
 * @param  {any} value
 * @returns any
 */
export declare function set(object: object, prop: string, value: any): void;
/**
 * @param  {Array<any>} objects
 * @returns any
 */
export declare function merge(objectA: any, objectB: any, objectC?: any, objectD?: any, objectE?: any, objectF?: any): any;
/**
 * @param  {string} message
 * @param  {object} extra
 * @returns any
 */
export declare function error(message: string, extra?: object, limit?: 8): any;
/**
 * @param  {any} arrayOrObject
 * @param  {Function} callback
 */
export declare function loopAsync(arrayOrObject: any, callback: Function): Promise<void>;
/**
 * @param  {number=2} concurrency
 * @param  {Array<any>} arr
 * @param  {Function} callback
 */
export declare function loopAsyncPool(concurrency: number, arr: Array<any>, callback: Function): Promise<any[]>;
declare let forEach: typeof loopAsync;
export { forEach };
/**
 * @param  {any} arrayOrObject
 * @param  {Function} callback
 */
export declare function loop(arrayOrObject: any, callback: Function): void;
/**
 * @param  {string} str
 * @returns string
 */
export declare function upperCase(str: string): string;
/**
 * @param  {string} str
 * @returns string
 */
export declare function lowerCase(str: string): string;
/**
 * @param  {string} str
 * @returns string
 */
export declare function sentenceCase(str: string): string;
/**
 * @param  {string} str
 * @returns string
 */
export declare function titleCase(str: string): string;
/**
 * @param  {string} str
 * @returns string
 */
export declare function startCase(str: string): string;
/**
 * @param  {string} str
 * @param  {object} options?
 * @returns str
 */
export declare function slug(str: string, options?: object): string;
/**
 * @param  {string} str
 * @returns str
 */
export declare function unslug(str: string): string;
/**
 * @param  {Function} callback
 * @param  {number=1} delay
 */
export declare function bgTask(callback: Function, delay?: number): Promise<void>;
/**
 * @param  {Array<any>} data
 * @param  {string=''} parentIdKey
 * @returns Array
 */
export declare function treeSort(data: any, parentIdKey: any): any[];
/**
 * @param  {number} n
 * @param  {string} single
 * @param  {string} multiple
 * @param  {string=null} zero
 */
export declare function howMany(n: number, single: string, multiple: string, zero?: string): string;
/**
 * @param  {number} num
 * @returns string
 */
export declare function numberToLetter(num: number): string;
/**
 * @param  {string} str
 * @returns number
 */
export declare function letterToNumber(str: string): number;
/**
 * @param  {string} str
 * @returns string
 */
export declare function nl2br(str: string): string;
/**
 * Check if given value is empty
 * @param  {any} value
 * @returns boolean
 */
export declare function isEmpty(value: any): boolean;
/**
 * @param  {Object|Array<any>|any} object
 * @returns Object|Array<any>|any
 */
export declare function clone(object: Object | Array<any> | any): Object | Array<any> | any;
/**
 * @param  {any} object
 * @returns string
 */
export declare function pack(value: any): string;
/**
 * @param  {string} object
 * @returns any
 */
export declare function unpack(value: string): any;
//# sourceMappingURL=index.d.ts.map