"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.unpack = exports.pack = exports.clone = exports.isEmpty = exports.nl2br = exports.letterToNumber = exports.numberToLetter = exports.howMany = exports.treeSort = exports.bgTask = exports.unslug = exports.slug = exports.startCase = exports.titleCase = exports.sentenceCase = exports.lowerCase = exports.upperCase = exports.loop = exports.forEach = exports.loopAsyncPool = exports.loopAsync = exports.error = exports.merge = exports.set = exports.get = exports.base64Decode = exports.base64Encode = exports.dateYear = exports.dateMonth = exports.dateWeek = exports.dateDay = exports.dateTimeZone = exports.dateTime = exports.time = exports.osDateJson = exports.date = exports.hashCode = exports.hash = exports.fixedNum = exports.formatAddress = exports.formatNum = exports.parseExcelNum = exports.parseNum = exports.logger = exports.setLogger = void 0;
var slugify_1 = require("slugify");
var setValue = require("lodash/set");
var getValue = require("lodash/get");
var mergeObjects = require("lodash/merge");
var _moment = require("moment");
var objectHash = require("object-hash");
var base64_1 = require("./base64");
var logger = console;
exports.logger = logger;
/**
 * @param  {} newLogger
 */
function setLogger(newLogger) {
    exports.logger = logger = newLogger;
}
exports.setLogger = setLogger;
/**
 * @param  {any} str
 * @returns number
 */
function parseNum(str) {
    if (!str)
        return 0;
    if (typeof str == "number")
        return str;
    if (typeof str.replace == 'function')
        str = Number(str.replace(/[^\d\.-]/g, ''));
    return parseFloat(str);
}
exports.parseNum = parseNum;
/**
 * @param  {any} str
 * @returns string
 */
function parseExcelNum(str) {
    // Scientific Notation to a String
    try {
        if (!str)
            return "";
        if (typeof str === "string")
            str = Number(str);
        str = str.toLocaleString('fullwide', { useGrouping: false });
        return str;
    }
    catch (e) {
        console.error(e);
    }
    return "";
}
exports.parseExcelNum = parseExcelNum;
/**
 * @param  {string|number} str
 * @param  {number=0} decimals
 * @param  {string=""} prefix
 * @param  {string=""} suffix
 * @returns string
 */
function formatNum(str, decimals, prefix, suffix) {
    if (decimals === void 0) { decimals = undefined; }
    if (prefix === void 0) { prefix = ""; }
    if (suffix === void 0) { suffix = ""; }
    var num = parseNum(str);
    var prettyDecimals = false;
    if (typeof decimals === "number" && decimals < 0) {
        decimals = Math.abs(decimals);
        prettyDecimals = true;
    }
    if (typeof decimals === "number") {
        num = Number(num.toFixed(decimals));
    }
    //adds commas
    var numStr = num + '';
    var split = numStr.split('.');
    var expression = /(\d+)(\d{3})/;
    var partA = split[0];
    var partB = split.length > 1 ? '.' + split[1] : '';
    if (!partB && typeof decimals === "number" && decimals && !prettyDecimals)
        partB = String(".").padEnd(decimals + 1, '0');
    while (expression.test(partA)) {
        partA = partA.replace(expression, '$1' + ',' + '$2');
    }
    numStr = partA + partB;
    return prefix + numStr + suffix;
}
exports.formatNum = formatNum;
/**
 * @param  {string|object|any} address
 * @param  {string="\n"} seperator
 * @returns string
 */
function formatAddress(address, seperator) {
    if (seperator === void 0) { seperator = "\n"; }
    var str = "";
    if (typeof address === "object") {
        var cityZip = [address.city, address.zipcode].filter(Boolean).join(", ");
        var stateCountry = [address.stateLong || address.state, address.countryLong || address.country].filter(Boolean).join(", ");
        var arr = [
            address.name,
            address.line1,
            address.line2,
            cityZip,
            stateCountry,
        ];
        str = arr.filter(Boolean).join(seperator);
    }
    else if (typeof address === "string") {
        str = address;
    }
    return str;
}
exports.formatAddress = formatAddress;
/**
 * @param  {string|number} num
 * @param  {number=2} decimals
 * @returns number
 */
function fixedNum(num, decimals) {
    if (decimals === void 0) { decimals = 2; }
    if (typeof num === "string")
        num = parseNum(num);
    if (typeof num === "undefined" || num === null)
        num = 0;
    num = Number(num.toFixed(decimals));
    return num;
}
exports.fixedNum = fixedNum;
/**
 * @param  {any} value
 * @returns string
 */
function hash(value) {
    if (value === null || value === undefined)
        value = "";
    if (typeof value === "object" || Array.isArray(value))
        value = JSON.stringify(value);
    return objectHash(value);
}
exports.hash = hash;
/**
 * @param  {any} value
 * @returns string
 */
function hashCode(value) {
    if (value === null || value === undefined)
        value = "";
    if (typeof value === "object" || Array.isArray(value))
        value = pack(value);
    var hash;
    for (var i = 0; i < value.length; i++) {
        hash = Math.imul(31, hash) + value.charCodeAt(i) | 0;
    }
    return hash ? hash.toString() : "";
}
exports.hashCode = hashCode;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
function date(str, outputFormat, inputFormat) {
    if (outputFormat === void 0) { outputFormat = ''; }
    if (inputFormat === void 0) { inputFormat = ''; }
    var date = _moment(str, inputFormat);
    return date.format(outputFormat || "D MMM YYYY");
}
exports.date = date;
/**
 * @param  {any} str
 * @returns string | Date
 */
function osDateJson(date) {
    if (typeof date === "string")
        date = new Date(date);
    return process.platform === "win32" ? date : date.toJSON();
}
exports.osDateJson = osDateJson;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
function time(str, outputFormat, inputFormat) {
    if (outputFormat === void 0) { outputFormat = ''; }
    if (inputFormat === void 0) { inputFormat = ''; }
    var time = _moment(str, inputFormat);
    return time.format(outputFormat || "h:mm A");
}
exports.time = time;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
function dateTime(str, outputFormat, inputFormat) {
    if (outputFormat === void 0) { outputFormat = ''; }
    if (inputFormat === void 0) { inputFormat = ''; }
    var time = _moment(str, inputFormat);
    return time.format(outputFormat || "D MMM YYYY h:mm A");
}
exports.dateTime = dateTime;
/**
 * @param  {any} str
 * @param  {string=''} outputFormat
 * @param  {string=''} inputFormat
 * @returns string
 */
function dateTimeZone(str, outputFormat, inputFormat) {
    if (outputFormat === void 0) { outputFormat = ''; }
    if (inputFormat === void 0) { inputFormat = ''; }
    var time = _moment(str, inputFormat);
    var tz = "";
    if (typeof str === "string") {
        tz = str.substr(-6);
    }
    return time.utcOffset(tz).format(outputFormat || "D MMM YYYY h:mm A Z");
}
exports.dateTimeZone = dateTimeZone;
/**
 * @param  {any} str
 * @returns string
 */
function dateDay(str) {
    var date = _moment(str).format("YYYY-MM-DD") + "T00:00:00+00:00";
    return date;
}
exports.dateDay = dateDay;
/**
 * @param  {any} str
 * @returns string
 */
function dateWeek(str) {
    var date = _moment(str).startOf('week').format("YYYY-MM-DD") + "T00:00:00+00:00";
    return date;
}
exports.dateWeek = dateWeek;
/**
 * @param  {any} str
 * @returns string
 */
function dateMonth(str) {
    var date = _moment(str).startOf('month').format("YYYY-MM-DD") + "T00:00:00+00:00";
    return date;
}
exports.dateMonth = dateMonth;
/**
 * @param  {any} str
 * @returns string
 */
function dateYear(str) {
    var date = _moment(str).startOf('year').format("YYYY-MM-DD") + "T00:00:00+00:00";
    return date;
}
exports.dateYear = dateYear;
/**
 * @param  {string} str
 * @returns string
 */
function base64Encode(str) {
    return base64_1.default.encode(str);
}
exports.base64Encode = base64Encode;
/**
 * @param  {string} str
 * @returns string
 */
function base64Decode(str) {
    return base64_1.default.decode(str);
}
exports.base64Decode = base64Decode;
/**
 * @param  {object} object
 * @param  {string} prop
 * @param  {any} def
 * @returns any
 */
function get(object, prop, def) {
    if (def === void 0) { def = undefined; }
    var current = object || {};
    if (!current || !prop || typeof prop != "string")
        return def;
    //lodash
    current = getValue(object, prop);
    // prop.split(".").forEach(function (key) {
    //     if (current && typeof current === "object" && current[key]) current = current[key];
    //     else current = undefined;
    // });
    if (typeof current === "boolean" || typeof current === "number")
        return current;
    return current || def;
}
exports.get = get;
/**
 * @param  {object} object
 * @param  {string} prop
 * @param  {any} value
 * @returns any
 */
function set(object, prop, value) {
    //lodash
    setValue(object, prop, value);
}
exports.set = set;
/**
 * @param  {Array<any>} objects
 * @returns any
 */
function merge(objectA, objectB, objectC, objectD, objectE, objectF) {
    var output = {};
    //lodash
    mergeObjects(output, objectA, objectB || {}, objectC || {}, objectD || {}, objectE || {}, objectF || {});
    return output;
}
exports.merge = merge;
/**
 * @param  {string} message
 * @param  {object} extra
 * @returns any
 */
function error(message, extra, limit) {
    var _this = this;
    Error.stackTraceLimit = limit || 8;
    Error.captureStackTrace(this, this.constructor);
    this.message = message;
    if (typeof extra == "object") {
        Object.keys(extra).map(function (key) {
            _this[key] = extra[key];
        });
    }
}
exports.error = error;
/**
 * @param  {any} arrayOrObject
 * @param  {Function} callback
 */
function loopAsync(arrayOrObject, callback) {
    return __awaiter(this, void 0, void 0, function () {
        var arr, index, obj, arr, index;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!Array.isArray(arrayOrObject)) return [3 /*break*/, 5];
                    arr = arrayOrObject;
                    index = 0;
                    _a.label = 1;
                case 1:
                    if (!(index < arr.length)) return [3 /*break*/, 4];
                    return [4 /*yield*/, callback(arr[index], index, arr)];
                case 2:
                    _a.sent();
                    _a.label = 3;
                case 3:
                    index++;
                    return [3 /*break*/, 1];
                case 4: return [3 /*break*/, 9];
                case 5:
                    if (!(typeof arrayOrObject === "object" || typeof arrayOrObject === "function")) return [3 /*break*/, 9];
                    obj = typeof arrayOrObject === "function" ? Object(arrayOrObject) : arrayOrObject;
                    arr = Object.keys(obj);
                    index = 0;
                    _a.label = 6;
                case 6:
                    if (!(index < arr.length)) return [3 /*break*/, 9];
                    return [4 /*yield*/, callback(obj[arr[index]], arr[index], obj)];
                case 7:
                    _a.sent();
                    _a.label = 8;
                case 8:
                    index++;
                    return [3 /*break*/, 6];
                case 9: return [2 /*return*/];
            }
        });
    });
}
exports.loopAsync = loopAsync;
/**
 * @param  {number=2} concurrency
 * @param  {Array<any>} arr
 * @param  {Function} callback
 */
function loopAsyncPool(concurrency, arr, callback) {
    if (concurrency === void 0) { concurrency = 2; }
    return __awaiter(this, void 0, void 0, function () {
        var pool, executing, _loop_1, index;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    pool = [];
                    executing = new Set();
                    _loop_1 = function (index) {
                        var p, clean;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    p = Promise.resolve().then(function () { return callback(arr[index], index, arr); });
                                    pool.push(p);
                                    executing.add(p);
                                    clean = function () { return executing.delete(p); };
                                    p.then(clean).catch(clean);
                                    if (!(executing.size >= concurrency)) return [3 /*break*/, 2];
                                    return [4 /*yield*/, Promise.race(executing)];
                                case 1:
                                    _b.sent();
                                    _b.label = 2;
                                case 2: return [2 /*return*/];
                            }
                        });
                    };
                    index = 0;
                    _a.label = 1;
                case 1:
                    if (!(index < arr.length)) return [3 /*break*/, 4];
                    return [5 /*yield**/, _loop_1(index)];
                case 2:
                    _a.sent();
                    _a.label = 3;
                case 3:
                    index++;
                    return [3 /*break*/, 1];
                case 4: return [2 /*return*/, Promise.all(pool)];
            }
        });
    });
}
exports.loopAsyncPool = loopAsyncPool;
//Alias for backword compatibility
var forEach = loopAsync;
exports.forEach = forEach;
/**
 * @param  {any} arrayOrObject
 * @param  {Function} callback
 */
function loop(arrayOrObject, callback) {
    if (Array.isArray(arrayOrObject)) {
        var arr = arrayOrObject;
        for (var index = 0; index < arr.length; index++) {
            callback(arr[index], index, arr);
        }
    }
    else if (typeof arrayOrObject === "object") {
        var obj = arrayOrObject;
        var arr = Object.keys(obj);
        for (var index = 0; index < arr.length; index++) {
            callback(obj[arr[index]], arr[index], obj);
        }
    }
    else if (typeof arrayOrObject === "function") {
        var obj = Object(arrayOrObject);
        var arr = Object.keys(obj);
        for (var index = 0; index < arr.length; index++) {
            callback(obj[arr[index]], arr[index], obj);
        }
    }
}
exports.loop = loop;
/**
 * @param  {string} str
 * @returns string
 */
function upperCase(str) {
    return str ? str.toUpperCase() : str;
}
exports.upperCase = upperCase;
/**
 * @param  {string} str
 * @returns string
 */
function lowerCase(str) {
    return str ? str.toLowerCase() : str;
}
exports.lowerCase = lowerCase;
/**
 * @param  {string} str
 * @returns string
 */
function sentenceCase(str) {
    return str ? str[0].toUpperCase() + str.slice(1) : str;
}
exports.sentenceCase = sentenceCase;
/**
 * @param  {string} str
 * @returns string
 */
function titleCase(str) {
    return str ? str.split(' ').map(function (s) { return s.charAt(0).toUpperCase() + s.substring(1); }).join(' ') : str;
}
exports.titleCase = titleCase;
/**
 * @param  {string} str
 * @returns string
 */
function startCase(str) {
    return str.replace(/([A-Z]+)/g, " $1").replace(/([A-Z][a-z])/g, " $1");
}
exports.startCase = startCase;
/**
 * @param  {string} str
 * @param  {object} options?
 * @returns str
 */
function slug(str, options) {
    var defaults = {
        replacement: '-',
        lower: true,
        remove: /[*+~.()'"!:@#?^\?\/\\\[\]]/g,
    };
    if (options)
        defaults = __assign(__assign({}, defaults), options);
    return str && typeof str === "string" ? (0, slugify_1.default)(str, defaults) : str;
}
exports.slug = slug;
/**
 * @param  {string} str
 * @returns str
 */
function unslug(str) {
    str = str
        .replace(/\-/g, " ")
        .replace(/\w\S*/g, function (text) { return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase(); });
    return str;
}
exports.unslug = unslug;
/**
 * @param  {Function} callback
 * @param  {number=1} delay
 */
function bgTask(callback, delay) {
    if (delay === void 0) { delay = 1; }
    return __awaiter(this, void 0, void 0, function () {
        var _this = this;
        return __generator(this, function (_a) {
            setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                var e_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, callback()];
                        case 1:
                            _a.sent();
                            return [3 /*break*/, 3];
                        case 2:
                            e_1 = _a.sent();
                            if (logger && typeof logger.log === "function")
                                logger.log(e_1);
                            else
                                console.log(e_1);
                            return [3 /*break*/, 3];
                        case 3: return [2 /*return*/];
                    }
                });
            }); }, delay);
            return [2 /*return*/];
        });
    });
}
exports.bgTask = bgTask;
/**
 * @param  {Array<any>} data
 * @param  {string=''} parentIdKey
 * @returns Array
 */
function treeSort(data, parentIdKey) {
    var all = __spreadArray([], data, true);
    var list = [];
    var idExists = {};
    var children = {};
    var mapDeep = function (record, index, order) {
        if (order === void 0) { order = false; }
        record.children = [];
        if (order)
            record.__order = index + 2;
        loop(children[record.id], function (child, childIndex) {
            if (child)
                record.children.push(mapDeep(child, childIndex));
        });
        return record;
    };
    loop(data, function (record, index) {
        if (record)
            idExists[record.id] = true;
    });
    loop(data, function (record, index) {
        if (record) {
            var parentId = record[parentIdKey];
            if (parentId && idExists[record.parentId]) {
                if (!children[parentId])
                    children[parentId] = [];
                children[parentId].push(record);
                all[index] = null;
            }
        }
    });
    var parents = all.filter(Boolean);
    loop(parents, function (record, index) {
        if (record)
            list.push(mapDeep(record, index));
    });
    return list;
}
exports.treeSort = treeSort;
/**
 * @param  {number} n
 * @param  {string} single
 * @param  {string} multiple
 * @param  {string=null} zero
 */
function howMany(n, single, multiple, zero) {
    if (zero === void 0) { zero = null; }
    var str = zero || multiple || single;
    if (n === 1)
        str = single;
    else if (n > 1)
        str = multiple;
    return str.replace("%s", n.toString());
}
exports.howMany = howMany;
/**
 * @param  {number} num
 * @returns string
 */
function numberToLetter(num) {
    var letters = '';
    while (num >= 0) {
        letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[num % 26] + letters;
        num = Math.floor(num / 26) - 1;
    }
    return letters;
}
exports.numberToLetter = numberToLetter;
/**
 * @param  {string} str
 * @returns number
 */
function letterToNumber(str) {
    var chrs = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ', mode = chrs.length - 1, number = 0;
    for (var p = 0; p < str.length; p++) {
        number = number * mode + chrs.indexOf(str[p]);
    }
    return number;
}
exports.letterToNumber = letterToNumber;
/**
 * @param  {string} str
 * @returns string
 */
function nl2br(str) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = '<br />';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
exports.nl2br = nl2br;
/**
 * Check if given value is empty
 * @param  {any} value
 * @returns boolean
 */
function isEmpty(value) {
    //if null or undefined = empty
    if (value === null || value === undefined)
        return true;
    //if array with length 0 = empty
    if (value.hasOwnProperty('length') && value.length === 0)
        return true;
    //if string of blank spaces = empty
    if (typeof value === "string" && !value.trim())
        return true;
    //if object with no keys = empty
    if (value.constructor === Object && Object.keys(value).length === 0)
        return true;
    // = not empty
    return false;
}
exports.isEmpty = isEmpty;
/**
 * @param  {Object|Array<any>|any} object
 * @returns Object|Array<any>|any
 */
function clone(object) {
    if (typeof structuredClone === "function")
        return structuredClone(object);
    else
        return JSON.parse(JSON.stringify(object));
}
exports.clone = clone;
/**
 * @param  {any} object
 * @returns string
 */
function pack(value) {
    return JSON.stringify(value);
}
exports.pack = pack;
/**
 * @param  {string} object
 * @returns any
 */
function unpack(value) {
    return JSON.parse(value);
}
exports.unpack = unpack;
